//the tricky part of this is that there is one without file extension 
tests: [
      { i: "getFileExtension('blatherskite.png');", o: 'png' },
      { i: "getFileExtension('perfectlylegal.torrent');", o: 'torrent' },
      { i: "getFileExtension('spaces are fine in file names.txt');", o: 'txt' },
      { i: "getFileExtension('this does not have one');", o: false },
      { i: "getFileExtension('.htaccess');", o: 'htaccess' }
    ],
function getFileExtension(i) {
    
    // i will be a string, but it may not have a file extension.
    // return the file extension (with no period) if it has one, otherwise false
    
    var filename = i.split('.');
    if(filename.length > 1){
        return filename[filename.length - 1];
    }
    else {
        return false;
    }
}


tests: [
  { i: "longestString(['a','ab','abc']);", o: 'abc' },
  { i: "longestString(['big',[0,1,2,3,4],'tiny']);", o: 'tiny' },
  { i: "longestString(['Hi','World','你好']);", o: 'World' },
  { i: "longestString([true,false,'lol']);", o: 'lol' },
  { i: "longestString([{object: true,mainly: 'to confuse you'},'x']);", o: 'x' }
],
function longestString(i) {
    
    // i will be an array.
    // return the longest string in the array
    var data = i, str = '', val;
    for(j = 0; j < data.length; j++){
        if(data[j].constructor === Array){
            continue;
            // if we want to go through arr elements we have to do it recursively
            // for(k = 0; k < data[j].length; k++){
            //     val = data[j];
            //     if(val.constructor){}
            //     console.log(val.length)
            // }
        }
        if(str.length < data[j].length){
            str = data[j];
        }
    }
    return str;
}

tests: [
      { i: "arraySum([1,2,3,4,5])", o: 15 },
      { i: "arraySum([[1,2,3],4,5])", o: 15 },
      { i: "arraySum([[1,2,false],'4','5'])", o: 3 },
      { i: "arraySum([[[[[[[[[1]]]]]]]], 1])", o: 2 },
      { i: "arraySum([['A','B','C','easy as',1,2,3]])", o: 6 }
    ],

function arraySum(i) {
    //i will be an array, containing integers, strings and/or arrays like itself.
    // Sum all the integers you find, anywhere in the nest of arrays.
    var res = 0;
    function getEveryEl(item){
        var val;
        for(var i = 0; i < item.length; i++){
            val = item[i];
            if(val.constructor === String){
                console.log(val)
            }
            else if(val.constructor === Number){
                res += val;
            }
            else if (val.constructor === Array){
                getEveryEl(val);
            }
        }
        return res;
    }
    return getEveryEl();
}

//This function can be used with 2 parameters
//its output has to be true for both conditions
//console.log(sum(2,3));   // Outputs 5
//console.log(sum(2)(3));  // Outputs 5
function sum(x){
  if (arguments.length == 2) {
    return arguments[0] + arguments[1];
  }
  else {
    return function(y){
      return x + y;
    }
  }
}
//or
function sum(x, y) {
  if (y !== undefined) {
    return x + y;
  } else {
    return function(y) { return x + y; };
  } 
}

//Simple function which check if the word is palindrome e.g. "A car, a man, a maraca"

function isPalindrome(str){
  str = str.replace('/\W/g', '').toLowerCase();
  return (str == str.split('').reverse().join(''));
};
//the 'i' variable will log different number when clicked on the button because we use closure
//otherwise without this anonymous function we will log '5' every time when click on some button
for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  btn.addEventListener('click', (function(i) {
    return function() { console.log(i); };
  })(i));
  //this will log 5 every time
  // btn.addEventListener('click', function() {
  //     console.log(i); 
  //   };
  // );
  document.body.appendChild(btn);
}
//this will double the array members
function doubleArray(arr) {
 var result = [],
  len = arr.length,
  resLen = arr.length * 2,
  i;

 for (i = 0; i < resLen; i++) {
  result[i] = arr[i % len];
 }

 return result;
}
//double Array 2
var dbl = [];
function double(arr, count){
    for(var i = 0; i < arr.length; i++){
        console.log(arr[i])
        dbl.push(arr[i])
    }
    if(count > 1){
        double(arr, count - 1)
    }
}
//best solution to check if value is int
function isInteger(x) { return (x^0) === x; } 

//recursive function for factoriel
(function f(n){
  return ((n > 1) ? n * f(n-1) : n)
})(10);