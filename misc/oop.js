//Inheritance and polymorphism
//Inheritance refers to an object being able to inherit methods and properties from a parent object 
//Polymorphism It is the practice of designing objects to share behaviors 
//and to be able to override shared behaviors with specific ones. 
//Polymorphism takes advantage of inheritance in order to make this happen.
//Person constructor
function Person(fname, lname){
	this.fname = fname;
	this.lname = lname;
};

Person.prototype.capitalizeFirstLetter = function(str){
	return str.charAt(0).toUpperCase() + str.slice(1);
};

Person.prototype.sayHello = function (){
	console.log('Hello, I am ' + this.capitalizeFirstLetter(this.fname) +' ' + this.capitalizeFirstLetter(this.lname));
};
//Employee constructor
function Employee (fname, lname, jobtitle, position){
	//we call the parent constructor
	Person.call(this, fname, lname);
	//init specific props for the class
	this.jobtitle = jobtitle;
	this.position = position
};

// Create a Employee.prototype object that inherits from Person.prototype.
// Note: A common error here is to use "new Person()" to create the
// Employee.prototype. That's incorrect for several reasons, not least 
// that we don't have anything to give Person for the "firstName" 
// argument. The correct place to call Person is above, where we call 
// it from Employee.

Employee.prototype = Object.create(Person.prototype);
//If we dont set the constructor to Employee, it will be Person's constructor
Employee.prototype.constructor = Employee;
//Override the parent method - polymorphism
Employee.prototype.sayHello = function(){
console.log('I can capitalize directly here if I use this.capitalizeFirstLetter and I dont have to ask 
	my parrent, so the person name will be written correctly ' + this.capitalizeFirstLetter(this.fname) + ' ' + this.capitalizeFirstLetter(this.lname) + ' 
	and he is working in the ' + this.jobtitle + ' sphere ');
}
//the use of capitalize func can be done only there, if i did this in the contructor it will show an error
var zhivko = new Employee('zhivko', 'dimitrov', 'IT', 'web developer');

//this is shim for Object.create
function createObject(proto) {
    function ctor() { }
    ctor.prototype = proto;
    return new ctor();
}

// Usage:
// Employee.prototype = createObject(Person.prototype);

//Encapsulation
//Encapsulation is the ability of an object to be a container 
//(or capsule) for its member properties, including variables and methods
//more often these props or methods are private
function Animal(name, age){
	this.name = name;
	this.age = age;
	var breed;
	var owner;
	this.getBreed = function(){return breed;};
	this.setBreed = function(newBreed){breed = newBreed;};
	this.getOwner = function() {return owner;};
	this.setOwner = function(newOwner) {owner = newOwner};
}
//or we can use more elegant way
function Animal(name, age){
	this.name = name;
	this.age = age;
	var breed;
	var owner;
	// var self = this;
	//Now this.name and this.age will be undefined unless if we asign this to self
	//and write methods to get the age and the name from self.
	return {
		getBreed:function(){return breed;},
		setBreed:function(newBreed){breed = newBreed;},
		getOwner:function() {return owner;},
		setOwner:function(newOwner) {owner = newOwner}
	}
}