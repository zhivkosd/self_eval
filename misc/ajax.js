function makeRequest() {
	var httpRequest = new XMLHttpRequest();
	// this is used for cross domain requests
	// var httpRequest = new XDomainRequest(); //if it is supported
	//first param is the method, second is the url
	httpRequest.open('GET', 'http://127.0.0.1:1122');
	//if the request is POST -> we can send data through send() method as first param
	httpRequest.send();

	//to handle the server response we have to assign function to :
	httpRequest.onreadystatechange = function(){
		//we have to chech if everything is ready
		if(httpRequest.readyState === XMLHttpRequest.DONE){
			//after that we have to chech the response
			if(httpRequest.status === 200){
				console.log('Response',httpRequest.responseText);
			}
		}
	};
};
