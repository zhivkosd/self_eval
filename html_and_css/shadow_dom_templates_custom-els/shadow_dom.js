var tmpl = document.querySelector('template'),
	host = document.querySelector('.img-slider'),
	root = host.createShadowRoot();

root.appendChild(document.importNode(tmpl.content, true));

// We can create our HTML5 element by using the new registerElement method
// First it is nesseccary to create a new object with HTMLElement prototype
var ImgSliderProto = Object.create(HTMLElement.prototype);

//Second we have to add createdCallback method - it is a good place to create our ShadowRoot

ImgSliderProto.createdCallback = function() {
	//here we have to create our shadow DOM and clone the template
	var privateRoot = this.createShadowRoot();
	privateRoot.appendChild(document.importNode(tmpl.content, true));
};

//Third we have to register our new element. The compulsory part is that the new el has to have '-' in his name

var ImgSlider = document.registerElement('img-slider',{
	prototype: ImgSliderProto
})

// few different ways to use it - use the <img-slider> tag somewhere in our HTML. 
//But we can also call document.createElement("img-slider") or 
//we can use the constructor that was returned by document.registerElement and stored in the ImgSlider variable